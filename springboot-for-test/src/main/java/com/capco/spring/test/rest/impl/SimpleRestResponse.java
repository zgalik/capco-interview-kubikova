package com.capco.spring.test.rest.impl;

import com.capco.spring.test.rest.api.RestResponse;

import lombok.Data;

@Data
public class SimpleRestResponse implements RestResponse {
	
	private String value;

}
