package com.capco.spring.test.rest.api;

public interface RestConstants {

	public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";
	
	public static final String ROOT_URL = "/rest";

}
