/**
 * 
 */
package com.capco.spring.test.service.exception;

import java.util.UUID;

import org.slf4j.helpers.MessageFormatter;

/**
 * Base exception which is used as supper class of more specific exceptions which may occur after service method execution.
 * Can provide correlation ID for better lookup in logs, see {@link #getCorrelationId()}.  
 * 
 * @author eedm
 *
 */
public class BaseServiceException extends RuntimeException {

	/**
	 * generated
	 */
	private static final long serialVersionUID = -3803045145249936102L;

	private String correlationId = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);

	public BaseServiceException() {
		super();
	}

	public BaseServiceException(String message) {
		super(message);
	}

	/**
	 * Use it like logging with SLF4J.
	 * 
	 * @param message specific exception message which can be formatted in same way as LOG message. See {@link MessageFormatter}.
	 * @param arguments an array of arguments to be substituted in the formatted message. See {@link MessageFormatter}.
	 */
	public BaseServiceException(String message, Object... arguments) {
		super(MessageFormatter.arrayFormat(message, arguments).getMessage());
	}

	public BaseServiceException(Throwable cause) {
		super(cause);
	}

	public BaseServiceException(Throwable cause, String message) {
		super(message, cause);
	}

	/**
	 * Use it like logging with SLF4J.
	 * 
	 * @param cause the cause
	 * @param message specific exception message which can be formatted in same way as LOG message. See {@link MessageFormatter}.
	 * @param arguments an array of arguments to be substituted in the formatted message. See {@link MessageFormatter}.
	 */
	public BaseServiceException(Throwable cause, String message, Object... arguments) {
		super(MessageFormatter.arrayFormat(message, arguments).getMessage(), cause);
	}

	/**
	 * Can be used on UI as correlation ID for better lookup in logs.
	 * 
	 * @return UUID as String
	 */
	public String getCorrelationId() {
		return correlationId;
	}
}
