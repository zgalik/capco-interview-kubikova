package com.capco.spring.test.rest.api.view;

import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

import com.capco.spring.test.rest.api.RestResponse;

@Data
public class ClientDTO implements Serializable, RestResponse {

	private int id;
	private String firstName;
	private String lastName;
    private String additionalField = "This is from View Object";
}
