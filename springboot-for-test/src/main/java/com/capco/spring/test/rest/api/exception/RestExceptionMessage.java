package com.capco.spring.test.rest.api.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.capco.spring.test.rest.api.RestResponse;

public class RestExceptionMessage implements RestResponse {
	/**
	 * Available message types.
	 *
	 */
	public enum MessageType {
		/**
		 * Formatted message based on this type can be shown in clients UI as
		 * Error.
		 */
		ERROR,
		/**
		 * Formatted message based on this type can be shown in clients UI as
		 * Warning.
		 */
		WARN,
		/**
		 * Formatted message based on this type can be shown in clients UI as
		 * Information.
		 */
		INFO
	}

	private MessageType messageType;
	private String messageId;
	private String correlationId;
	private List<? extends Serializable> details;
	private String message;
	private String technicalMessage;

	public RestExceptionMessage(String messageId) {
		super();
		this.details = new ArrayList<>();
		this.messageId = messageId;
		this.messageType = MessageType.ERROR;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getMessageId() {
		return messageId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public List<? extends Serializable> getDetails() {
		return details;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTechnicalMessage() {
		return technicalMessage;
	}

	public void setTechnicalMessage(String technicalMessage) {
		this.technicalMessage = technicalMessage;
	}

	@Override
	public String toString() {
		return "ExceptionObject [messageId=" + messageId + ", messageType=" + messageType + ", technicalMessage="
				+ technicalMessage + ", details=" + details + "]";
	}

}
