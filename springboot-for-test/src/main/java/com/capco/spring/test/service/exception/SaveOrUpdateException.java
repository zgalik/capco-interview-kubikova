package com.capco.spring.test.service.exception;

public class SaveOrUpdateException extends BaseServiceException {

	/**
	 * Generated
	 */
	private static final long serialVersionUID = 1557385449560652307L;

	public SaveOrUpdateException() {
		super();
	}

	/**
	 * More info: {@link BaseServiceException#BaseServiceException(String, Object...)}
	 */
	public SaveOrUpdateException(String message, Object... arguments) {
		super(message, arguments);
	}

	public SaveOrUpdateException(String message) {
		super(message);
	}

	/**
	 * More info: {@link BaseServiceException#BaseServiceException(Throwable, String, Object...)}
	 */
	public SaveOrUpdateException(Throwable cause, String message, Object... arguments) {
		super(cause, message, arguments);
	}

	public SaveOrUpdateException(Throwable cause, String message) {
		super(cause, message);
	}

	public SaveOrUpdateException(Throwable cause) {
		super(cause);
	}

}
