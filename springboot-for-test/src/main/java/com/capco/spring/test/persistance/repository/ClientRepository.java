package com.capco.spring.test.persistance.repository;

import com.capco.spring.test.persistance.model.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.Set;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {

	@Query(value = "FROM Client c WHERE c.id = ?1")
	Client getClientById(int id);

	@Query(value = "FROM Client")
	Set<Client> getAllClients();
}
