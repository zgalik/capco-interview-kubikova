package com.capco.spring.test.service.api;

import com.capco.spring.test.persistance.model.Client;

import java.util.Set;

public interface ClientService {

	public Client getById(int id);
	
	public Client getAllById(int id);
	
	public Client save(Client client);

	public Set<Client> getAll();
}
