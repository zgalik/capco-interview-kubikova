package com.capco.spring.test.service.impl;

import com.capco.spring.test.service.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capco.spring.test.persistance.model.Client;
import com.capco.spring.test.persistance.repository.ClientRepository;
import com.capco.spring.test.service.api.ClientService;
import com.capco.spring.test.service.exception.ProcessingException;

import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

	private final ClientRepository clientRepository;

	@Autowired
	public ClientServiceImpl(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}

	@Override
	public Client getById(int id) {
		Client findOne;
		try {
			findOne = clientRepository.findOne(id);
		} catch (Exception e) {
			log.error("Unable to find client by id:{}", id, e);
			throw new ProcessingException(e, "Client not found under id:{}", id);
		}
		if (findOne == null) {
			throw new NotFoundException("Client not found under id:{}", id);
		}
		return findOne;
	}

	@Override
	public Client save(Client client) {
		return clientRepository.save(client);
	}

	@Override
	public Set<Client> getAll() {
		return clientRepository.getAllClients();
	}

	@Override
	public Client getAllById(int id) {
		return clientRepository.getClientById(id);
	}

}
