package com.capco.spring.test.rest.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.capco.spring.test.rest.api.RestConstants;
import com.capco.spring.test.service.api.ClientService;
import com.capco.spring.test.service.exception.NotFoundException;
import org.dozer.Mapper;
import com.capco.spring.test.persistance.model.Client;
import com.capco.spring.test.rest.api.RestResponse;
import com.capco.spring.test.rest.api.exception.RestExceptionMessage;
import com.capco.spring.test.rest.api.view.ClientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

import static com.capco.spring.test.rest.impl.ClientControllerImpl.MAIN_URL;


@RestController
@Slf4j
@RequestMapping(RestConstants.ROOT_URL + MAIN_URL)
public class ClientControllerImpl {

	static final String MAIN_URL = "/client";
	private static final String PATH_VARIABLE_CLIENT_ID = "id";

	private final ClientService clientService;
	private final Mapper mapper;

	@Autowired
	public ClientControllerImpl(ClientService clientService, Mapper mapper) {
		this.clientService = clientService;
		this.mapper = mapper;
	}

	@RequestMapping(value = "/all-in-map", method = RequestMethod.GET, produces = RestConstants.APPLICATION_JSON_CHARSET_UTF_8)
	public ResponseEntity<Map<String, String>> getAllClients() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		log.trace("calling getAllClients() method. Principal: {}", auth.getPrincipal());

		Map<String, String> result = new HashMap<>();
		result.put("ID1", "Allianz");
		result.put("ID2", "Generalli");

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json", produces = RestConstants.APPLICATION_JSON_CHARSET_UTF_8)
	public ResponseEntity<RestResponse> addClient(@RequestHeader("c-token") String token, @RequestBody ClientDTO client) {
		log.trace("calling addClient() method with token: {}, client: {}", token, client);

		if (!"capco-IW-token".equals(token)) {
			log.error("Token is not matching");
			RestExceptionMessage rem = new RestExceptionMessage("INVALID_TOKEN");
			rem.setCorrelationId("1234-4321");
			rem.setTechnicalMessage("Token is not matching");
			return new ResponseEntity<RestResponse>(rem, HttpStatus.NOT_FOUND);
		}

		Client clientToSave = mapper.map(client, Client.class);
		clientToSave.setId(0);
		clientService.save(clientToSave);

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/get" + "/{" + PATH_VARIABLE_CLIENT_ID
			+ "}", method = RequestMethod.GET, produces = RestConstants.APPLICATION_JSON_CHARSET_UTF_8)
	ResponseEntity<RestResponse> getClient(@PathVariable(value = PATH_VARIABLE_CLIENT_ID) String id) {
		log.trace("calling getClient() method with id: {}", id);
		Client client;
		try {
			client = clientService.getById(Integer.parseInt(id));
		} catch (NotFoundException e) {
			log.error("Exception occured.", e);
			RestExceptionMessage rem = new RestExceptionMessage("ID_NOT_FOUND");
			rem.setCorrelationId(e.getCorrelationId());
			rem.setTechnicalMessage(e.getMessage());
			return new ResponseEntity<>(rem, HttpStatus.NOT_FOUND);
		}

		ClientDTO result = mapper.map(client, ClientDTO.class);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = RestConstants.APPLICATION_JSON_CHARSET_UTF_8)
	public ResponseEntity<List<Client>> getClients() {
		final Set<Client> all = clientService.getAll();
		return new ResponseEntity<>(new ArrayList<>(all), HttpStatus.OK);
	}

}
