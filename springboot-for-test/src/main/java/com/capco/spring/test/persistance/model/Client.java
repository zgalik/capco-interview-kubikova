package com.capco.spring.test.persistance.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Data;
import lombok.ToString;

import java.util.Set;


/**
 * The persistent class for the client database table.
 * 
 */
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NamedQuery(name="Client.findAll", query="SELECT c FROM Client c")
public class Client extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="LAST_NAME")
	private String lastName;

}