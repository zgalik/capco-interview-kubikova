package com.capco.spring.test.service.exception;

/**
 * Service exception which can indicate exception when processing some task: e.g. serialization/deserialization to/from JSON object, etc.
 * 
 * @author eedm
 *
 */
public class ProcessingException extends BaseServiceException {

	/**
	 * generated
	 */
	private static final long serialVersionUID = -4057243355335767262L;

	public ProcessingException() {
		super();
	}

	public ProcessingException(String message) {
		super(message);
	}

	/**
	 * More info: {@link BaseServiceException#BaseServiceException(String, Object...)}
	 */
	public ProcessingException(String message, Object... arguments) {
		super(message, arguments);
	}

	public ProcessingException(Throwable cause) {
		super(cause);
	}

	public ProcessingException(Throwable cause, String message) {
		super(cause, message);
	}

	/**
	 * More info: {@link BaseServiceException#BaseServiceException(Throwable, String, Object...)}
	 */
	public ProcessingException(Throwable cause, String message, Object... arguments) {
		super(cause, message, arguments);
	}

}
