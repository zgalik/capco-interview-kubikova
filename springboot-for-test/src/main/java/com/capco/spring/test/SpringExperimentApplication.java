package com.capco.spring.test;

import java.util.Arrays;

import com.capco.spring.test.persistance.model.Client;
import com.capco.spring.test.persistance.repository.ClientRepository;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class SpringExperimentApplication {

	private ClientRepository clientRepository;

	public static void main(String[] args) {
		final ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringExperimentApplication.class, args);

		final String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
		Arrays.stream(beanDefinitionNames).sorted().forEach(s -> {
			log.debug("Bean definition: {}", s);
		});
	}

	@Bean
	public CommandLineRunner loadData(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;

		return (String... strings) -> initData();
	}

	private void initData() {
		log.trace("Calling initData method");
		DataFactory df = new DataFactory();

		for (int i = 0; i < 10; i++) {
			Client client = new Client();
			client.setFirstName(df.getFirstName());
			client.setLastName(df.getLastName());
			clientRepository.save(client);
		}

	}
}
