package com.capco.spring.test.rest.api;

import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping(RestConstants.ROOT_URL + UserRestService.MAIN_URL)
public interface UserRestService {

	public static final String MAIN_URL = "/user";
	
	@RequestMapping(value = "/current", method = RequestMethod.GET, produces = RestConstants.APPLICATION_JSON_CHARSET_UTF_8)
	public ResponseEntity<RestResponse> getCurrentUser(Principal principal);
}
