/**
 * 
 */
package com.capco.spring.test.service.exception;

/**
 * Service exception which can indicate: e.g. entity was not found after executing DB select query. 
 * 
 * @author eedm
 *
 */
public class NotFoundException extends BaseServiceException {

	/**
	 * generated
	 */
	private static final long serialVersionUID = -5710203664042036174L;

	public NotFoundException() {
		super();
	}

	public NotFoundException(String message) {
		super(message);
	}

	/**
	 * More info: {@link BaseServiceException#BaseServiceException(String, Object...)}
	 */
	public NotFoundException(String message, Object... arguments) {
		super(message, arguments);
	}

	public NotFoundException(Throwable cause) {
		super(cause);
	}

	public NotFoundException(Throwable cause, String message) {
		super(cause, message);
	}

	/**
	 * More info: {@link BaseServiceException#BaseServiceException(Throwable, String, Object...)}
	 */
	public NotFoundException(Throwable cause, String message, Object... arguments) {
		super(cause, message, arguments);
	}

}
