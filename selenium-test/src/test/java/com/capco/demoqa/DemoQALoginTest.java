package com.capco.demoqa;

import com.capco.demoqa.login.Login;
import com.capco.demoqa.pages.LoginPage;
import com.capco.demoqa.pages.ProfilePage;
import com.capco.utils.DriverUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class DemoQALoginTest {
    private WebDriver driver;

    @Before
    public void setDriver() throws Exception {
        driver = DriverUtil.getWebdriver();
        driver.manage().window().maximize();
    }

    @Test
    public void loginToDemoQAWithCapcoUser() {
        //1. Go to DemoQA website
        LoginPage loginPage = Login.goToLoginPage(driver);

        //2. Login with CAPCO credentials
        loginPage.fillInUsernameAndPassword(driver, "capcotest100", "Capcotest#100123");
        ProfilePage profilePage = loginPage.clickLogin(driver);

        //3. Verify successful login
        //TODO

        //4. Sign out from DemoQA
        //TODO

        //5. Verify successful logout
        //TODO
    }

    @After
    public void closeDriver() {
        driver.close();
    }
}
