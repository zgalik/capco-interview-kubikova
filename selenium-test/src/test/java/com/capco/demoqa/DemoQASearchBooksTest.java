package com.capco.demoqa;

import com.capco.demoqa.login.Login;
import com.capco.demoqa.pages.BookDescriptionPage;
import com.capco.demoqa.pages.BookstorePage;
import com.capco.demoqa.pages.LoginPage;
import com.capco.demoqa.pages.ProfilePage;
import com.capco.utils.DriverUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class DemoQASearchBooksTest {
    private WebDriver driver;

    @Before
    public void setDriver() throws Exception {
        driver = DriverUtil.getWebdriver();
        driver.manage().window().maximize();
    }

    @Test
    public void loginToDemoQAWithCapcoUser() {
        //1. Go to DemoQA website
        LoginPage loginPage = Login.goToLoginPage(driver);

        //2. Login with CAPCO credentials and check successful login through assertion
        loginPage.fillInUsernameAndPassword(driver, "capcotest100", "Capcotest#100123");
        ProfilePage profilePage = loginPage.clickLogin(driver);
        //TODO

        //3. Go to Bookstore page and search for JAVA
        BookstorePage bookstorePage = Login.goToBookstorePage(driver);
        //TODO

        //4. Click "Eloquent JavaScript, Second Edition" book for more details
        //TODO

        //5. Check the "Description" contains "JavaScript is a flexible" string
        //TODO

        //6. Go back to Bookstore page and click Logout
        //TODO

    }

    @After
    public void closeDriver() {
        driver.close();
    }
}
