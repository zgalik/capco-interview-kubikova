package com.capco.gmail.pages;

import com.capco.utils.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class SignInPage {
    public void fillInUsernameAndClickNext(WebDriver driver, String username) {
        WebUtil.clearAndSendKeys(driver, By.id("identifierId"), username);
        WebUtil.click(driver, By.id("identifierNext"));
    }

    public void fillInPassword(WebDriver driver, String password) {
        //TODO
    }

    public EmailHomePage clickPasswordNextAndSignIn(WebDriver driver) {
        //TODO
        return PageFactory.initElements(driver, EmailHomePage.class);
    }

}
