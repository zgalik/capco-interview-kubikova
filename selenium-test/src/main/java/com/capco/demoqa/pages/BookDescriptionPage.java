package com.capco.demoqa.pages;

import com.capco.utils.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookDescriptionPage {

    public String getBookDescription(WebDriver driver) {
        //TODO - please use WebUtil pre-defined functions
        return "";
    }

    public BookstorePage clickBackToBookstore(WebDriver driver) {
        //TODO - please use WebUtil pre-defined functions
        return PageFactory.initElements(driver, BookstorePage.class);
    }

}
