package com.capco.demoqa.pages;

import com.capco.utils.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage {

    public boolean verifySuccessfulLogin(WebDriver driver) {
        //TODO - please use WebUtil pre-defined functions
        return true;
    }

    public LoginPage clickLogout(WebDriver driver) {
        //TODO - please use WebUtil pre-defined functions
        return PageFactory.initElements(driver, LoginPage.class);
    }

}
