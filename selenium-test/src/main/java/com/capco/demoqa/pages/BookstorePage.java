package com.capco.demoqa.pages;

import com.capco.utils.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BookstorePage {

    public void searchForString(WebDriver driver, String text) {
        //TODO - please use WebUtil pre-defined functions
    }

    public BookDescriptionPage clickBookName(WebDriver driver, String bookName) {
        //TODO - please use WebUtil pre-defined functions
        return PageFactory.initElements(driver, BookDescriptionPage.class);
    }

    public LoginPage clickLogout(WebDriver driver) {
        //TODO - please use WebUtil pre-defined functions
        return PageFactory.initElements(driver, LoginPage.class);
    }

}
