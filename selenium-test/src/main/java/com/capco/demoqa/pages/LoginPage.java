package com.capco.demoqa.pages;

import com.capco.utils.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    public void fillInUsernameAndPassword(WebDriver driver, String username, String password) {
        //TODO - please use WebUtil pre-defined functions

    }

    public ProfilePage clickLogin(WebDriver driver) {
        //TODO - please use WebUtil pre-defined functions
        return PageFactory.initElements(driver, ProfilePage.class);
    }

    public boolean verifyUserNotLoggedIn(WebDriver driver) {
        //TODO - please use WebUtil pre-defined functions
        return true;
    }

}
