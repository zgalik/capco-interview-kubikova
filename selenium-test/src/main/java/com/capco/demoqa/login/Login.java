package com.capco.demoqa.login;

import com.capco.demoqa.pages.BookstorePage;
import com.capco.demoqa.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Login {

    public static LoginPage goToLoginPage(WebDriver driver) {
        driver.get("http://demoqa.com/login");
        return PageFactory.initElements(driver, LoginPage.class);
    }

    public static BookstorePage goToBookstorePage(WebDriver driver) {
        driver.get("http://demoqa.com/books");
        return PageFactory.initElements(driver, BookstorePage.class);
    }

}
