package com.capco.utils;

import com.capco.gmail.pages.SignInPage;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebUtil {
    private final static int WAIT_TIME_OUT = 30;

    public static SignInPage goToSignInPage(WebDriver driver) {
        driver.get("https://mail.google.com");
        return PageFactory.initElements(driver, SignInPage.class);
    }

    public static void click(WebDriver driver, By locator) {
        WebElement element = driver.findElement(locator);
        element.click();
    }

    public static void javascriptClick(WebDriver driver, By locator) {
        //FYI:
        //Element is present but having permanent Overlay.
        //We need to use JavascriptExecutor to send the click directly on the element.
        //https://stackoverflow.com/questions/44912203/selenium-web-driver-java-element-is-not-clickable-at-point-x-y-other-elem

        WebElement elementToClick = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", elementToClick);
    }

    public static void clearAndSendKeys(WebDriver driver, By locator, String keys) {
        WebElement element = driver.findElement(locator);
        element.clear();
        element.sendKeys(keys);
    }

    public static void pressEnter(WebDriver driver, By locator) {
        WebElement element = driver.findElement(locator);
        element.sendKeys(Keys.RETURN);
    }

    public static boolean isElementDisplayed(WebDriver driver, By locator) {
        boolean result;
        try {
            result = driver.findElement(locator).isDisplayed();
        } catch (NoSuchElementException e) {
            result = false;
        }
        return result;
    }

    public static void waitForElementVisible(WebDriver driver, By locator) {
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_OUT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static boolean isElementExist(WebDriver driver, By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public static String getElementText(WebDriver driver, By locator) {
        WebElement element = driver.findElement(locator);
        return element.getText();
    }

}
