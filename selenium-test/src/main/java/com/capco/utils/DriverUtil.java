package com.capco.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.nio.file.Path;
import java.nio.file.Paths;

public class DriverUtil {
    private static String OS = System.getProperty("os.name").toLowerCase();

    public static WebDriver getWebdriver() throws Exception {
        if (isUnix()) {
            Path webdriverUnixPath = Paths.get("src", "test", "resources", "chromedriver");
            System.setProperty("webdriver.chrome.driver", webdriverUnixPath.toString());
            return new ChromeDriver();
        } else if (isWindows()) {
            Path webdriverWinPath = Paths.get("src", "test", "resources", "chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", webdriverWinPath.toString());
            return new ChromeDriver();
        } else {
            throw new Exception("No such OS implementation for webdriver!");
        }
    }

    private static boolean isWindows() {
        return (OS.contains("win"));
    }

    private static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
    }
}
