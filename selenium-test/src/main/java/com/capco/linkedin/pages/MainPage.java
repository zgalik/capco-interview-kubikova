package com.capco.linkedin.pages;

import com.capco.utils.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class MainPage {

	public boolean verifySuccessfulLogin(WebDriver driver) {

		return true;
	}

	public boolean verifySuccessfulLogout(WebDriver driver) {

		return true;
	}

	public MainPage signOutFromLinkedIn(WebDriver driver) {

		return PageFactory.initElements(driver, MainPage.class);
	}

	public MainPage searchText(WebDriver driver, String searchJobsText, String searchLocationText) {

		return PageFactory.initElements(driver, MainPage.class);
	}

	public MainPage clickNavTitle(WebDriver driver, String navTitle) {

		return PageFactory.initElements(driver, MainPage.class);
	}


}
