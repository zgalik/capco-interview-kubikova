package com.capco.linkedin.pages;

import com.capco.utils.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	public MainPage fillInUsernameAndPassword(WebDriver driver, String username, String password) {
		WebUtil.waitForElementVisible(driver, By.id("username"));

		return PageFactory.initElements(driver, MainPage.class);
	}

}
