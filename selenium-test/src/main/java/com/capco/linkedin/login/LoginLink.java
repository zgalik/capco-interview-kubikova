package com.capco.linkedin.login;

import com.capco.linkedin.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginLink {

	public static LoginPage goToLoginPage(WebDriver driver) {
		driver.get("https://www.linkedin.com/login");
		return PageFactory.initElements(driver, LoginPage.class);
	}

}
