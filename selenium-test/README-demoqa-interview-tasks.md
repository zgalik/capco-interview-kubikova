### CAPCO Selenium interview tasks 

Goal is to evaluate the candidate's skills of Selenium webdriver.

**DESCRIPTION**
- Simulation of how test automation of DemoQA.com page could look like by using the following technologies: Java, Selenium webdriver, Maven, JUnit. 
- The project is based on page object pattern. 
- A test account is created on DemoQA.com/login with username **capcotest100** and password **Capcotest#100123**

### Task 01 - DemoQA Sign-in test
- Within your solution use the already pre-defined test methods 
1) Go to DemoQA.com webpage 
2) Login with CAPCO credentials
    - Use the already registered username **capcotest100** and password **Capcotest#100123**
3) Verify successful login
    - Use assertion for verification 
4) Sign out from the page (Log out button)
5) Verify successul logout
    - Use assertion for verification

### Task 02 - DemoQA search for a specific book and check the correct description
- Within your solution use the already pre-defined test methods
1) Go to DemoQA.com webpage 
2) Login with CAPCO credentials
    - Use the already registered username **capcotest100** and password **Capcotest#100123**
3) Verify successful login
    - Use assertion for verification 
4) On "Book Store" page search for "Java" keyword
5) From listed books click on "Eloquent JavaScript, Second Edition" book name
6) Verify the book description contains "JavaScript is a flexible" keyword (string)
    - Use assertion for verification   
7) Sign out from the page (Log out button)
8) Verify successul logout
    - Use assertion for verification

### Task 03 - Code optimization
1) The 2 tests above are far not optimal. 
    - What kind of optimizations would you suggest to this project? 
    - What are the drawbacks of the current solution? 