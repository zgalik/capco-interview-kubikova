### CAPCO REST API interview tasks 

Goal is to evaluate the skills of a candidate of REST APIs with RestAssured (http://rest-assured.io/)

### Task 01 - IngRestApiTest
**DESCRIPTION**
- Within your solution use the pre-defined jUnit test methods in `capco-automation-interview/rest-test/src/test/java/com/capco/rest/test/IngRestApiTest.java` 
- Use RestAssured Java library to get and validate response from [ING data](https://www.ing.nl/api/locator/atms/)

### TASKS
1) Check valid endpoint response
    - Check HTTP status code of the response from [ING data](https://www.ing.nl/api/locator/atms/) (should be 200 - OK). 
    - **Use method:** GIVEN_valid_request_WHEN_get_response_from_ING_THEN_valid_response_received()

2) Validate JSON response and count the number of objects
    - Get valid JSON response from response body [ING data](https://www.ing.nl/api/locator/atms/). Remove special characters in first line, due to [XSSI](https://security.stackexchange.com/questions/110539/how-does-including-a-magic-prefix-to-a-json-response-work-to-prevent-xssi-attack?noredirect=1&lq=1) 
    - Count the number of objects within valid JSON body 
    - **Use method:** GIVEN_valid_request_WHEN_exctract_JSON_body_from_response_THEN_valid_JSON_object_received()

3) Convert JSON response to Java objects and filter them
    - Convert the valid JSON response from response body [ING data](https://www.ing.nl/api/locator/atms/) to Java objects. 
    - Use the pre-defined objects at `capco-automation-interview/rest-test/src/main/java/com/capco/rest/data/` 
    - Filter out **city=Rhoon** from _address_ element
    - Assert that filtered result is **size()=3**
    - **Use method:** GIVEN_valid_JSON_body_WHEN_convert_JSON_to_java_objects_THEN_valid_objects_received_with_city_Rhoon_equal_3()

### Task 02 - EndpointRestApiTest
**DESCRIPTION and SETUP**
- Use command line and navigate to _springboot-for-test_ folder. From there execute `mvn spring-boot:run`. Wait until application starts and try to access [login](http://localhost:8080/) => Login screen shout appear.
- Use following credentials on login screen, **username=fero**, **password=fero123**. After successful login you should see a page with 
    > Hello fero!
- Within your solution use the pre-defined jUnit test methods in `capco-automation-interview/rest-test/src/test/java/com/capco/rest/test/EndpointRestApiTest.java`
- There are 2 endpoints to be tested: [add client](http://localhost:8080/rest/client/add) and [fetch all clients](http://localhost:8080/rest/client/all)
    
### TASKS
1) Authentication and login check
    - Create a private method to login to [login](http://localhost:8080/login) (credentials **username=fero**, **password=fero123**) 
    - Obtain _JSESSIONID_ cookie from response
    - **Use method:** authenticateAndGetJSessionIdCookie()

2) Add new client 
    - Use the authentication method and cookie value from Task 1) to login [login](http://localhost:8080/login)
    - Add the following header `c-token` into request. To populate this header with correct value, read content of file located at `capco-automation-interview/rest-test/src/test/resources/c-token-value`. 
    - Also add headers _Accept_ and _Content-Type_ = application/json.
    - Using POST method add the following client (JSON body below) into datastore through endpoint [Add client](http://localhost:8080/rest/client/add) 
     ``json
     {
         "firstName": "Jano",
         "lastName": "Dodo"
     }
    ``  
    - Evaluate the response status (HTTP Status = 201 Created) 
    - **Use method:** GIVEN_new_clients_available_WHEN_new_clients_added_to_datastore_THEN_clients_succesfully_added()

3) Fetch all clients  
    - Use the authentication method and cookie value from Task 1) to login [login](http://localhost:8080/login)
    - Using GET method on endpoint [Fetch all clients](http://localhost:8080/rest/client/all) fetch all clients from datasource
    - Check if the client "Jano" "Dodo" appeared within the client list
    - **Use method:** GIVEN_new_clients_added_to_datastore_WHEN_listing_all_clients_THEN_new_clients_appear_within_the_list()

=====================

### Prerequisites
- at least JDK 1.8 needs to be installed (tested with Java 8)
- Maven 3.3+ is required
- GIT client 2.7+
- IntelliJ IDEA (or your preferred Java IDE)

### Project structure description
- _rest-test_ folder contains Maven structure with dependencies like JUnit, RestAssured, etc. This project should be used for REST API testing.
- _springboot-for-test_ folder contains Maven structure with Spring-boot project. Spring-boot application is used for testing exposed REST endpoints (accessible from http://localhost:8080)
