package com.capco.rest.test;

import com.capco.rest.data.IngDataObject;
import com.google.gson.Gson;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

public class IngRestApiTest {

    @Test
    public void GIVEN_valid_request_WHEN_get_response_from_ING_THEN_valid_response_received() {
        //TODO: Complete the endpoint call from IngRestApiTest => Task 1
    }

    @Test
    public void GIVEN_valid_request_WHEN_exctract_JSON_body_from_response_THEN_valid_JSON_object_received() {
        //TODO: Complete the endpoint call from IngRestApiTest => Task 2

        String jsonBody = "...";

        try {
            JSONArray responseArray = new JSONArray(jsonBody);
            assertNotNull(responseArray);
            assertTrue(responseArray.length() > 0);
            System.out.println("Number of objects within JSON body: " + responseArray.length());
        } catch (JSONException ex) {
            fail(ex.getLocalizedMessage());
        }
    }

    @Test
    public void GIVEN_valid_JSON_body_WHEN_convert_JSON_to_java_objects_THEN_valid_objects_received_with_city_Rhoon_equal_3() {
        //TODO: IngRestApiTest => Task 2
    }
}