package com.capco.rest.test;

import io.restassured.RestAssured;
import io.restassured.http.Cookie;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.StringContains.containsString;

public class EndpointRestApiTest {

    @Test
    public void GIVEN_new_clients_available_WHEN_new_clients_added_to_datastore_THEN_clients_succesfully_added() {
        String newClient =
                "{\n" +
                        "\"firstName\": \"Jano\",\n" +
                        "\"lastName\": \"Dodo\"\n" +
                        "}";

        //TODO: EndpointRestApiTest => Task 02
    }

    @Test
    public void GIVEN_new_clients_added_to_datastore_WHEN_listing_all_clients_THEN_new_clients_appear_within_the_list() {
        //TODO: EndpointRestApiTest => Task 03
    }

    private Cookie authenticateAndGetJSessionIdCookie() {
        RestAssured.baseURI = "http://localhost:8080/";
        //TODO: EndpointRestApiTest => Task 01
        return null;
    }
}
